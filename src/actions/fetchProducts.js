import { createAction } from 'redux-actions';
import { FETCH_PRODUCTS } from './../constants';
import { apiGet } from './../api';
import { urlProducts } from './../api/urls';

export const fetchProducts = createAction(FETCH_PRODUCTS, apiGet(urlProducts));