import { createAction } from 'redux-actions';
import { ADD_TO_CART } from './../constants';

export const addToCart = createAction(ADD_TO_CART, (product) => product);