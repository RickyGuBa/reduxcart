import { createAction } from 'redux-actions';
import { FETCH_CART } from './../constants';

export const fetchCart = createAction(FETCH_CART, () => [] );