import { createAction } from 'redux-actions';
import { DELETE_FROM_CART } from './../constants';

export const deleteFromCart = createAction(DELETE_FROM_CART, (id) => id);