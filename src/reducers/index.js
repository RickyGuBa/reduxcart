import { combineReducers } from "redux";
import { productos } from "./productos";
import { cart } from "./cart";

export default combineReducers({
    productos,
    cart
})