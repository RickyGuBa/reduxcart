import { handleActions } from "redux-actions";
import { ADD_TO_CART, FETCH_CART, DELETE_FROM_CART } from './../constants';

export const cart = handleActions({
    [ADD_TO_CART]: (state, action) => [...state, action.payload],
    [FETCH_CART]: (state, action) => [...action.payload],
    [DELETE_FROM_CART]: (state, action) => state.filter(p => p.id !== action.payload)
}, [])