import { handleActions } from "redux-actions";
import { FETCH_PRODUCTS } from "../constants";

export const productos = handleActions({
    [FETCH_PRODUCTS]: (state, action) => [...action.payload],
}, []);