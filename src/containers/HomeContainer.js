import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from "react-bootstrap";
import { connect } from 'react-redux';

import { fetchProducts } from './../actions/fetchProducts';
import { addToCart } from './../actions/insertCart';
import { fetchCart } from './../actions/fetchCart';
import { deleteFromCart } from './../actions/deleteFromCart';

import ProductList from '../components/ProductList';
import ShoppingCart from '../components/ShoppingCart';
import { getProductos } from '../selectors/productos';
import { getCart } from '../selectors/cart';

class HomeContainer extends Component {

    componentDidMount() {
        if(this.props.productos.length === 0){
            this.props.fetchProducts();
            this.props.fetchCart();
        }

    }

    handleAddToCart = (p) => {
        this.props.addToCart(p);
    }

    handleDeleteFromCart = (id) => {
        this.props.deleteFromCart(id);
    }

    render() {
        return (
            <div className="HomeContainer">
                <Row>
                    <Col xs={12} md={8}>
                        <ProductList productos={this.props.productos} addToCart={this.handleAddToCart} />
                    </Col>
                    <Col xs={12} md={4}>
                        <ShoppingCart cart={this.props.cart} deleteFromCart={this.handleDeleteFromCart} />
                    </Col>
                </Row>
            </div>
        );
    }
}

HomeContainer.propTypes = {
    fetchProducts: PropTypes.func.isRequired,
    productos: PropTypes.array.isRequired,
    addToCart: PropTypes.func.isRequired,
    fetchCart: PropTypes.func.isRequired,
};

HomeContainer.defaultProps = {
    productos: [],
    cart: []
}

const mapStateToProps = (state) => ({
    productos: getProductos(state),
    cart: getCart(state)
})

export default connect(mapStateToProps, { fetchProducts, addToCart, fetchCart, deleteFromCart })(HomeContainer);