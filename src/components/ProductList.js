import React from 'react';
import PropTypes from 'prop-types';
import ProductItem from './ProductItem';

const ProductList = ({ productos, addToCart }) => {
    return (
        <div className="ProductList">
            {
                productos.map((c) => 
                    <ProductItem key={c.id} id={c.id} nombre={c.nombre} precio={c.precio} img={c.img} addToCart={addToCart} />
                )
            }
        </div>
    );
};

ProductList.propTypes = {
    productos: PropTypes.array.isRequired,
    addToCart: PropTypes.func.isRequired,
};

export default ProductList;