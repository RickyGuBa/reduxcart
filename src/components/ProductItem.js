import React from 'react';
import PropTypes from 'prop-types';
import { Col, Thumbnail, Button } from "react-bootstrap";
import { IoIosCart } from "react-icons/io";

const ProductItem = ({ id, nombre, precio, img, addToCart }) => {

    const addProduct = () => {
        const p = {
            id: Date.now(),
            nombre,
            precio
        }
        addToCart(p);
    }

    return (
        <div className="ProductItem">
            <Col xs={6} md={4}>
                <Thumbnail src={img}>
                    <h3>{nombre}</h3>
                    <p>${precio.toFixed(2)}</p>
                    <p>
                        <Button bsStyle="primary" onClick={() => addProduct()}><IoIosCart /></Button>
                    </p>
                </Thumbnail>
            </Col>
        </div>
    );
};

ProductItem.propTypes = {
    id: PropTypes.number.isRequired,
    nombre: PropTypes.string.isRequired,
    precio: PropTypes.number.isRequired,
};

export default ProductItem;