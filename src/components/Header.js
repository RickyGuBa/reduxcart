import React from 'react';
import { Navbar } from 'react-bootstrap'

const Header = () => {
    return (
        <div className="Header">
            <Navbar bsStyle="inverse">
                <Navbar.Header >
                    <Navbar.Brand>
                    <a href="/">Store</a>
                    </Navbar.Brand>
                </Navbar.Header>
            </Navbar>
        </div>
    );
};

export default Header;