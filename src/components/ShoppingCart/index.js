import React from 'react';
import PropTypes from 'prop-types';
import { Panel, Table, Button, Glyphicon  } from "react-bootstrap";

const ShoppingCart = ({ cart, deleteFromCart }) => {

    const deleteProduct = (id) => {
        deleteFromCart(id);
    }

    return (
        <div className="ShoppingCart">
            <Panel header="Shopping Cart" bsStyle="primary">
                <Panel.Heading>Shopping Cart</Panel.Heading>
                <Table>
                    <tbody>
                        {
                            cart.map((p) => 
                                <tr key={p.id}>
                                    <td>{p.nombre}</td>
                                    <td className="text-right">${p.precio.toFixed(2)}</td>
                                    <td className="text-right"><Button bsSize="small" bsStyle="danger" onClick={() => deleteProduct(p.id)}><Glyphicon glyph="trash" /></Button></td>
                                </tr>
                            )
                        }
                    </tbody>
                    <tfoot>
                    <tr>
                        <td className="total" colSpan="4" >
                            Total: ${cart.reduce((sum, product) => sum + product.precio, 0).toFixed(2)}
                        </td>
                    </tr>
                    </tfoot>
                </Table>
            </Panel>
            
        </div>
    );
};

ShoppingCart.propTypes = {
    cart: PropTypes.array.isRequired,
    deleteFromCart: PropTypes.func.isRequired,
};

export default ShoppingCart;